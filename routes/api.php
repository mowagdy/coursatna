<?php

Route::group([
    'namespace' => 'Api',
    'as' => 'api.',
    'prefix' => 'v1',
], function () {

    /*
    |--------------------------------------------------------------------------
    | Guest Endpoints
    |--------------------------------------------------------------------------
    */

    // GENERAL
    Route::group(['namespace' => 'General', 'prefix' => 'general'], function () {
        Route::get('levels', 'LevelController@index');
        Route::get('subjects', 'SubjectController@index');
    });

    // LOCATIONS
    Route::group(['namespace' => 'Locations', 'prefix' => 'locations'], function () {
        Route::get('governorates', 'GovernorateController@index');
        Route::get('governorates/{governorate}/cities', 'GovernorateCityController@index');
        Route::get('cities', 'CityController@index');
    });
//    Route::group(['namespace' => 'Courses', 'prefix' => 'courses'], function () {
//        Route::get('/', 'CourseController@index');
//        Route::get('{course}', 'CourseController@show');
//    });
    // AUTH
    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
        Route::post('phone/register', 'PhoneRegisterController@register');
        Route::post('phone/login', 'PhoneLoginController@login');
        Route::post('logout', 'LogoutController@logout')->middleware('auth:api');
        // Verification
        Route::post('phone/verification-resend', 'PhoneVerificationController@resendCode');
        Route::post('phone/verify', 'PhoneVerificationController@verifyUser');
        // ForgotPassword
        Route::post('password/forgot', 'ForgotPasswordController@forgotPassword');
        Route::post('password/verify-code', 'ForgotPasswordController@verifyCode');
        Route::post('password/set', 'ForgotPasswordController@setNewPassword')->middleware('auth:api');
    });


    /*
    |--------------------------------------------------------------------------
    | Student Endpoints
    |--------------------------------------------------------------------------
    */
    Route::group([
        'as' => 'auth.',
        'middleware' => ['auth:api'],
    ], function () {

        // SETTINGS
        Route::group(['namespace' => 'Settings', 'prefix' => 'settings'], function () {
            Route::put('/', 'SettingController@updateProfile');
            Route::put('password', 'SettingController@updatePassword');
            Route::put('level', 'SettingController@updateLevel');
            Route::post('avatar', 'SettingController@uploadAvatar');
        });

        // COURSES
        Route::group(['namespace' => 'Courses', 'prefix' => 'courses'], function () {
            Route::get('/', 'CourseController@index');
            Route::get('{course}', 'CourseController@show');
        });
    });

});
