<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group([
    'prefix' => 'doctor-panel',
    'namespace' => 'Doctor',
    'as' => 'doctor.',
    'middleware' => ['auth', 'role:UNIVERSITY_DOCTOR'],
], function () {
    Route::get('/', 'HomeController@index');
    Route::get('index', 'HomeController@index')->name('index');

    Route::resource('items/{item}/authors', 'ItemAuthorController');
    Route::get('items/{item}/paid_transactions', 'PaidTransactionController@index')
        ->name('items.paid_transactions');
    Route::resource('items', 'ItemController');

    Route::resource('announcements', 'AnnouncementController');
});


Route::group([
    'prefix' => 'creator-student',
    'namespace' => 'CreatorStudent',
    'as' => 'creator-student.',
    'middleware' => ['auth', 'role:UNIVERSITY_CREATOR_STUDENT'],
], function () {
    Route::get('/', 'HomeController@index');
    Route::get('index', 'HomeController@index')->name('index');

    Route::resource('items/{item}/authors', 'ItemAuthorController');
    Route::get('items/{item}/paid_transactions', 'PaidTransactionController@index')
        ->name('items.paid_transactions');

    Route::resource('items', 'ItemController');
});


Route::group([
    'prefix' => 'dashboard',
    'namespace' => 'Dashboard',
    'as' => 'dashboard.',
    'middleware' => ['auth', 'permission:access-dashboard'],
], function () {
    Route::get('/', 'HomeController@index');
    Route::get('index', 'HomeController@index')
        ->name('index');

    Route::resource('fields', 'FieldController');
    Route::get('levels', 'LevelController@index')
        ->name('levels.index');

    Route::resource('countries',
        'CountryController');
    Route::resource('countries/{country?}/governorates',
        'GovernorateController');
    Route::resource('countries/{country?}/governorates/{governorate?}/cities',
        'CityController');

    Route::get('json/countries', 'CountryController@jsonCountries')
        ->name('json.countries');
    Route::get('json/governorates', 'GovernorateController@jsonGovernorates')
        ->name('json.governorates');
    Route::get('json/cities', 'CityController@jsonCities')
        ->name('json.cities');

    Route::resource('universities',
        'UniversityController');
    Route::resource('universities/{university?}/faculties',
        'FacultyController');
    Route::resource('universities/{university?}/faculties/{faculty?}/majors',
        'MajorController');

    Route::get('json/universities', 'UniversityController@jsonUniversities')
        ->name('json.universities');
    Route::get('json/faculties', 'FacultyController@jsonFaculties')
        ->name('json.faculties');
    Route::get('json/majors', 'MajorController@jsonMajors')
        ->name('json.majors');

    Route::resource('universities/{university?}/faculties/{faculty?}/university-subjects',
        'UniversitySubjectController');
    //Route::resource('university-subjects', 'UniversitySubjectController');

    Route::get('json/university-subjects', 'UniversitySubjectController@jsonUniversitySubjects')
        ->name('json.university-subjects');

    Route::resource('school-subjects', 'SchoolSubjectController');

    Route::get('items/{item}/pdf', 'ItemController@viewPdf')
        ->name('items.viewPdf');
    Route::resource('items', 'ItemController');

    Route::resource('notifications', 'NotificationController')
        ->except(['show', 'edit', 'update', 'destroy']);

    Route::resource('charging-codes', 'ChargingCodeController');

    Route::get('charging-codes-files', 'ChargingCodesFileController@index')
        ->name('charging_codes_files.index');
    Route::get('charging-codes-files/{file}', 'ChargingCodesFileController@download')
        ->name('charging_codes_files.download');

    Route::get('users/export', 'UserController@export')
        ->name('users.export');
    Route::get('users/roles', 'UserController@roles')
        ->name('users.roles');
    Route::get('users/{user}/add-balance', 'UserController@addBalanceView')
        ->name('users.addBalanceView');
    Route::post('users/{user}/add-balance', 'UserController@addBalance')
        ->name('users.addBalance');
    Route::post('users/{user}/ban', 'UserController@ban')
        ->name('users.ban');
    Route::resource('users', 'UserController');
    Route::resource('doctors', 'DoctorController');

    Route::get('creator-students/requests', 'CreatorStudentController@requests')
        ->name('creator-students.requests');
    Route::get('creator-students/export', 'CreatorStudentController@export')
        ->name('creator-students.export');
    Route::resource('creator-students', 'CreatorStudentController');

    Route::resource('sys-users', 'SysUserController');

    Route::resource('rooms', 'RoomController');
    Route::get('rooms/{roomId}/requests/pending', 'RoomRequestController@pending')
        ->name('rooms.requests.pending');
    Route::get('rooms/{roomId}/requests/{requestId}/accept', 'RoomRequestController@accept')
        ->name('rooms.requests.accept');
    Route::get('rooms/{roomId}/requests/{requestId}/reject', 'RoomRequestController@reject')
        ->name('rooms.requests.reject');
    //Route::resource('conversations', 'ConversationController');

    Route::resource('banned-words', 'BannedWordController');

    Route::get('contact-us', 'ContactController@index')
        ->name('contact-us.index');
});


Auth::routes(['register' => false, 'verify' => true]);

Route::get('/home', 'HomeController@index')
    ->name('home');
