@extends('layouts.creator-student-master')

@section('title','Manage Items')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Manage Items</h1></div>
        @include('creator-student.common._alert_message')
        <div class="section-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Items <span>({{ $total }})</span></h4>
                            <div class="card-header-action">
                                <a href="{{route('creator-student.items.create')}}"
                                   class="btn btn-primary">Add <i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive table-invoice">
                                @if(count($items)>0)
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th>Level</th>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Creator</th>
                                            <th>Price</th>
                                            <th>Version</th>
                                            <th></th>
                                        </tr>
                                        @foreach($items as $item)
                                            <tr>
                                                <td>{{ $item->level }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->type }}</td>
                                                <td>{{ $item->creator->name }}</td>
                                                <td>{{ $item->price }}</td>
                                                <td>{{ $item->version }}</td>

                                                <td class="text-right">
                                                    <a href="{{route('creator-student.items.show',$item->id)}}"
                                                       class="btn btn-success">
                                                        <i class="fa fa-eye"></i>
                                                    </a>

                                                    <a href="{{route('creator-student.items.edit',$item->id)}}"
                                                       class="btn btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <button class="btn btn-danger delete"
                                                            data-id="{{$item->id}}" type="button">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="text-center p-3 text-muted">
                                        <h5>No Results</h5>
                                        <p>Looks like you have not added any items yet!</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(count($items)>0)
                        <div class="text-center">
                            {{$items->links()}}
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    @include('creator-student.common._modal_delete')
@endsection
