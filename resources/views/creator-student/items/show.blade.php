@extends('layouts.creator-student-master')

@section('title',$item->name)

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>item: {{$item->name}}</h1>
        </div>

        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-shopping-bag"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>إجمالي النسخ المباعة</h4>
                        </div>
                        <div class="card-body">
                            {{$paid_transactions_count}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-dollar-sign"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>إجمالي حصتي</h4>
                        </div>
                        <div class="card-body">
                            {{$doctor_total_money}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-warning">
                        <i class="fas fa-wallet"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>رصيدي الحالي</h4>
                        </div>
                        <div class="card-body">
                            {{$doctor_current_balance}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="section-body">
            <div class="card card-primary">
                <div class="card-body">

                    <div class="card-stats">
                        <div class="card-stats-items">
                            <div class="card-stats-item">
                                <div class="card-stats-item-label">level</div>
                                <div class="card-stats-item-count">{{$item->level}}</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-label">Subject</div>
                                <div class="card-stats-item-count">{{$item->universitySubject->name}}</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-label">Type</div>
                                <div class="card-stats-item-count">{{$item->type}}</div>
                            </div>
                            <div class="card-stats-item">
                                <div class="card-stats-item-label">Price</div>
                                <div class="card-stats-item-count">{{$item->price}}</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-8 col-md-12 col-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Paid Transactions</h4>
                    </div>
                    <div class="card-body">
                        @if(count($paid_transactions)>0)
                            <ul class="list-unstyled list-unstyled-border">
                                @foreach($paid_transactions as $transaction)
                                    <li class="media">
                                        <img class="mr-3 rounded-circle" width="50"
                                             src="{{asset('assets/dashboard/img/avatar/avatar-1.png')}}" alt="avatar">
                                        <div class="media-body">
                                            <div class="float-right">{{$transaction->paid_at}}</div>
                                            <div class="media-title">{{$transaction->user->name}}</div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        @else
                            No data available
                        @endif
                        <div class="text-center pt-1 pb-1">
                            <a href="{{route('creator-student.items.paid_transactions',$item->id)}}"
                               class="btn btn-primary btn-lg btn-round">View All</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
