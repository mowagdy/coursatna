@extends('layouts.dashboard-master')

@section('title','Manage Countries')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Manage Countries</h1></div>
        @include('dashboard.common._alert_message')
        <div class="section-body">
            @can('view-countries')
                <div class="card">
                    <div class="card-header">
                        <h4>Countries <span>({{ $total }})</span></h4>
                        <div class="card-header-action">
                            @can('create-countries')
                                <a href="{{route('dashboard.countries.create')}}"
                                   class="btn btn-primary">Add <i class="fas fa-plus"></i></a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive table-invoice">
                            @if(count($countries)>0)
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <th>Name</th>

                                        <th></th>
                                    </tr>
                                    @foreach($countries as $country)
                                        <tr>
                                            <td>{{ $country->name }}</td>
                                            <td class="text-right">
                                                @can('view-governorates')
                                                    <a href="{{route('dashboard.governorates.index',
                                                ['countryId'=>$country->id])}}" class="btn btn-dark">
                                                        Governorates <i class="far fa-folder-open"></i>
                                                    </a>
                                                @endcan
                                                @can('edit-countries')
                                                    <a href="{{route('dashboard.countries.edit',$country->id)}}"
                                                       class="btn btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan
                                                @can('delete-countries')
                                                    <button class="btn btn-danger delete"
                                                            data-id="{{$country->id}}" type="button">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text-center p-3 text-muted">
                                    <h5>No Results</h5>
                                    <p>Looks like you have not added any countries yet!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @if(count($countries)>0)
                    <div class="text-center">
                        {{$countries->links()}}
                    </div>
                @endif
            @endcan
        </div>
    </section>
@endsection

@section('scripts')
    @include('dashboard.common._modal_delete')
@endsection
