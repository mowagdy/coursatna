@if(session('message'))
    <div class="alert {{ session('class') }} alert-dismissible show fade">
        <div class="alert-body">
            <button class="close" data-dismiss="alert"><span>×</span></button>
            {{ session('message') }}
        </div>
    </div>
@endif
