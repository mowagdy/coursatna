@extends('layouts.dashboard-master')

@section('title','Create Item')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Add Item</h1></div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    @include('dashboard.common._alert_message')
                    @include('dashboard.common._alert_validation_errors')
                    <div class="card">
                        <div class="card-header">
                            <h4>Add a New Item</h4>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('dashboard.items.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                {{-- Name --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Item
                                        Name</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="name" value="{{old('name')}}"
                                               class="form-control @error('name') is-invalid @enderror">
                                        @error('name')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('name') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                {{-- Type --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('type') is-invalid @enderror" name="type"
                                                id="item_type">
                                            @foreach(config("enums.item_types") as $key=>$value)
                                                <option>{{$key}}</option>
                                            @endforeach
                                        </select>
                                        @error('type')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('type') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Item For --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Item
                                        Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="item_for_university" name="item_for"
                                                   value="UNIVERSITY" class="custom-control-input"
                                                {{ old('item_for') == 'UNIVERSITY' ? ' checked' : ''}}>
                                            <label class="custom-control-label" for="item_for_university">University
                                                Subject</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="item_for_school" name="item_for"
                                                   value="SCHOOL" class="custom-control-input"
                                                {{ old('item_for') == 'SCHOOL' ? ' checked' : ''}}>
                                            <label class="custom-control-label" for="item_for_school">School
                                                Subject</label>
                                        </div>
                                    </div>
                                </div>

                                {{-- Country --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Country</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('country_id') is-invalid @enderror"
                                                name="country_id" id="countries_list">
                                            @foreach($countries as $country)
                                                <option value="{{$country->id}}"
                                                    {{ old('country_id') ==$country->id ? ' selected' : ''}}>{{$country->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('country_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('country_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                {{-- City --}}
                                <div class="form-group row mb-4 item_university">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">City</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('city_id') is-invalid @enderror"
                                                name="city_id" id="cities_list">
                                            @foreach($cities as $city)
                                                <option value="{{$city->id}}"
                                                    {{ old('city_id') ==$city->id ? ' selected' : ''}}>{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('city_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('city_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- University --}}
                                <div class="form-group row mb-4 item_university">
                                    <label
                                        class="col-form-label text-md-right col-12 col-md-3 col-lg-3">University</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('university_id') is-invalid @enderror"
                                                name="university_id" id="universities_list">
                                            @foreach($universities as $university)
                                                <option value="{{$university->id}}"
                                                    {{ old('university_id') ==$university->id ? ' selected' : ''}}>{{$university->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('university_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('university_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Faculty --}}
                                <div class="form-group row mb-4 item_university">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Faculty</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('faculty_id') is-invalid @enderror"
                                                name="faculty_id" id="faculties_list">
                                            @foreach($faculties as $faculty)
                                                <option value="{{$faculty->id}}"
                                                    {{ old('faculty_id') ==$faculty->id ? ' selected' : ''}}>{{$faculty->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('faculty_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('faculty_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Level --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Level</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('level') is-invalid @enderror"
                                                name="level" id="levels_list">
                                            @foreach($levels as $level)
                                                <option>{{$level->label}}</option>
                                            @endforeach
                                        </select>
                                        @error('level')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('level') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- School Subjects --}}
                                <div class="form-group row mb-4 item_school">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">School
                                        Subject</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('school_subject_id') is-invalid @enderror"
                                                name="school_subject_id">
                                            @foreach($schoolSubjects as $schoolSubject)
                                                <option value="{{$schoolSubject->id}}">{{$schoolSubject->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('school_subject_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('school_subject_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Univerity Subjects --}}
                                <div class="form-group row mb-4 item_university">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">University
                                        Subject</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select
                                            class="form-control @error('university_subject_id') is-invalid @enderror"
                                            name="university_subject_id" id="university_subjects_list">
                                            @foreach($universitySubjects as $universitySubject)
                                                <option
                                                    value="{{$universitySubject->id}}">{{$universitySubject->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('university_subject_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('university_subject_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4" id="item_file_dev">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">File</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="file" value="{{old('file')}}"
                                               class="form-control @error('file') is-invalid @enderror">
                                        @error('file')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('file') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4" id="external_url_dev">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">External
                                        URL</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="url" name="external_url" value="{{old('external_url')}}"
                                               class="form-control @error('external_url') is-invalid @enderror">
                                        @error('external_url')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('external_url') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="image" value="{{old('image')}}"
                                               class="form-control @error('image') is-invalid @enderror">
                                        @error('image')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('image') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Author</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="author" value="{{old('author')}}"
                                               class="form-control @error('author') is-invalid @enderror">
                                        @error('author')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('author') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Price</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" step="0.01" name="price" value="{{old('price')}}"
                                               class="form-control @error('price') is-invalid @enderror">
                                        @error('price')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('price') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                                           for="description">Description</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('description') is-invalid @enderror"
                                                  id="description" name="description"
                                                  rows="4">{{old('description')}}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('description') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            /* -------------------------//
            // --- Change item Type --- //
            //------------------------- */
            const liveUrlConst = "{{config('enums.item_types.LIVE_URL')}}";
            $("#item_type").change(function () {
                const itemType = $("#item_type option:selected").val();
                if (itemType === liveUrlConst) {
                    $('#item_file_dev').hide();
                } else {
                    $('#item_file_dev').show();
                }
            });


            /* -------------------------//
            // --- Select Item Type --- //
            //------------------------- */

            //  select item type at initialization
            let itemFor = $("input[name='item_for']:checked").val();
            selectItemFor(itemFor);
            hydrateLevels(itemFor);

            //  select item type on change
            $("input[type=radio][name='item_for']").change(function () {
                itemFor = $("input[name='item_for']:checked").val();
                selectItemFor(itemFor);
                hydrateLevels(itemFor);
            });

            function selectItemFor(itemFor) {
                if (itemFor === 'UNIVERSITY') {
                    $('.item_university').show();
                    $('.item_school').hide();
                } else if (itemFor === 'SCHOOL') {
                    $('.item_university').hide();
                    $('.item_school').show();
                }
            }


            /* -----------------------//
            // --- Hydrate Levels --- //
            //----------------------- */
            function hydrateLevels(itemFor) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: "GET",
                    data: {type: itemFor},
                    url: '{{route('dashboard.levels.index')}}',
                    dataType: 'json',
                    success: function (data) {
                        const $el = $("#levels_list");
                        $el.empty(); // remove old options
                        $.each(data, function (value, key) {
                            $el.append($("<option></option>").attr("value", key.label).text(key.label));
                        });
                    }
                });
            }

            /* -----------------------//
            // --- Hydrate Cities --- //
            //----------------------- */
            $('#countries_list').on('change', function (e) {
                const countryIdSelected = this.value;
                hydrateCities(countryIdSelected);
            });

            function hydrateCities(countryId) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: "GET",
                    data: {countryId: countryId},
                    url: '{{route('dashboard.json.cities')}}',
                    dataType: 'json',
                    success: function (data) {
                        const $el = $("#cities_list");
                        $el.empty(); // remove old options
                        $.each(data, function (value, key) {
                            $el.append($("<option></option>").attr("value", key.id).text(key.name));
                        });

                        hydrateUniversities($('#cities_list').val());
                    }
                });
            }

            /* ---------------------------//
            // -- Hydrate Universities -- //
            //--------------------------- */
            $('#cities_list').on('change', function (e) {
                const cityIdSelected = this.value;
                hydrateUniversities(cityIdSelected);
            });

            function hydrateUniversities(cityId) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: "GET",
                    data: {cityId: cityId},
                    url: '{{route('dashboard.json.universities')}}',
                    dataType: 'json',
                    success: function (data) {
                        const $el = $("#universities_list");
                        $el.empty(); // remove old options
                        $.each(data, function (value, key) {
                            $el.append($("<option></option>").attr("value", key.id).text(key.name));
                        });

                        hydrateFaculties($('#universities_list').val());
                    }
                });
            }

            /* --------------------------//
            // --- Hydrate Faculties --- //
            //-------------------------- */
            $('#universities_list').on('change', function (e) {
                const universityIdSelected = this.value;
                hydrateFaculties(universityIdSelected);
            });

            function hydrateFaculties(universityId) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: "GET",
                    data: {universityId: universityId},
                    url: '{{route('dashboard.json.faculties')}}',
                    dataType: 'json',
                    success: function (data) {
                        const $el = $("#faculties_list");
                        $el.empty(); // remove old options
                        $.each(data, function (value, key) {
                            $el.append($("<option></option>").attr("value", key.id).text(key.name));
                        });

                        hydrateUniversitySubjects($('#faculties_list').val());
                    }
                });
            }

            /* ---------------------------------//
            // -- Hydrate UniversitySubjects -- //
            //--------------------------------- */
            $('#faculties_list').on('change', function (e) {
                const facultyIdSelected = this.value;
                hydrateUniversitySubjects(facultyIdSelected);
            });

            function hydrateUniversitySubjects(facultyId) {
                $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                $.ajax({
                    type: "GET",
                    data: {facultyId: facultyId},
                    url: '{{route('dashboard.json.university-subjects')}}',
                    dataType: 'json',
                    success: function (data) {
                        const $el = $("#university_subjects_list");
                        $el.empty(); // remove old options
                        $.each(data, function (value, key) {
                            $el.append($("<option></option>").attr("value", key.id).text(key.name));
                        });
                    }
                });
            }

        });
    </script>
@endsection
