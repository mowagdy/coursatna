@extends('layouts.doctor-master')

@section('title','Attach Author')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Attach Author to item</h1></div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    @include('doctor.common._alert_message')
                    <div class="card">
                        <div class="card-header">
                            <h4>Attach Author to the item: {{$item->name}}</h4>
                        </div>
                        <div class="card-body">

                            <form method="POST" action="{{ route('doctor.authors.store',$item->id) }}"
                                  enctype="multipart/form-data">
                                @csrf

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Author</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('author_id') is-invalid @enderror"
                                                name="author_id" >
                                            @foreach($doctors as $doctor)
                                                <option value="{{$doctor->id}}">{{$doctor->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('author_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('author_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Points</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" name="points" value="{{old('points',1)}}"
                                               class="form-control @error('points') is-invalid @enderror">
                                        @error('points')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('points') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                         <button type="submit" class="btn btn-primary">Attach</button>
                                         <a href="{{route('doctor.items.show',['item'=>$item->id])}}"
                                            class="btn btn-danger">Cancel</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
