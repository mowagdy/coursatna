@extends('layouts.doctor-master')

@section('title','Manage Authors')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Manage Authors of: {{$item->name}}</h1></div>
        @include('doctor.common._alert_message')
        <div class="section-body">
            <div class="card">
                <div class="card-header">
                    <h4>Authors <span>({{ $total }})</span></h4>
                    <div class="card-header-action">
                        <a href="{{route('doctor.items.show',$item->id)}}" class="btn btn-primary">Back to the item</a>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive table-invoice">
                        @if(count($authors)>0)
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th>Points</th>
                                    <th></th>
                                </tr>
                                @foreach($authors as $author)
                                    <tr>
                                        <td>{{ $author->author->name }}</td>
                                        <td>{{ $author->points }}</td>
                                        <td class="text-right">
                                            {{--<a href="{{route('doctor.authors.edit',$author->id)}}"
                                               class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button class="btn btn-danger delete"
                                                    data-id="{{$author->id}}" type="button">
                                                <i class="fa fa-trash"></i>
                                            </button>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="text-center p-3 text-muted">
                                <h5>No Results</h5>
                                <p>Looks like you have not added any authors yet!</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
