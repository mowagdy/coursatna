@extends('layouts.doctor-master')

@section('title','Manage Announcements')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Manage Announcements</h1></div>
        @include('doctor.common._alert_message')
        <div class="section-body">
            @can('view-announcements')
                <div class="card">
                    <div class="card-header">
                        <h4>Announcements <span>({{ $total }})</span></h4>
                        <div class="card-header-action">
                            @can('create-announcements')
                                <a href="{{route('doctor.announcements.create')}}"
                                   class="btn btn-primary">Add <i class="fas fa-plus"></i></a>
                            @endcan
                        </div>
                    </div>
                    <div class="card-body p-0">
                        <div class="table-responsive table-invoice">
                            @if(count($announcements)>0)
                                <table class="table table-striped">
                                    <tbody>
                                    <tr>
                                        <th>Level</th>
                                        <th>Announcement</th>
                                        <th></th>
                                    </tr>
                                    @foreach($announcements as $announcement)
                                        <tr>
                                            <td>{{ $announcement->level }}</td>
                                            <td>{{ $announcement->text }}</td>
                                            <td class="text-right">
                                                @can('edit-announcements')
                                                    <a href="{{route('doctor.announcements.edit',$announcement->id)}}"
                                                       class="btn btn-primary">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                @endcan
                                                @can('delete-announcements')
                                                    <button class="btn btn-danger delete"
                                                            data-id="{{$announcement->id}}" type="button">
                                                        <i class="fa fa-trash"></i>
                                                    </button>
                                                @endcan
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @else
                                <div class="text-center p-3 text-muted">
                                    <h5>No Results</h5>
                                    <p>Looks like you have not added any announcements yet!</p>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                @if(count($announcements)>0)
                    <div class="text-center">
                        {{$announcements->links()}}
                    </div>
                @endif
            @endcan
        </div>
    </section>
@endsection

@section('scripts')
    @include('doctor.common._modal_delete')
@endsection
