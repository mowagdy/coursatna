@extends('layouts.doctor-master')

@section('title','Edit Announcement, '.$announcement->name)

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Edit Announcement</h1>
        </div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    @include('doctor.common._alert_message')
                    <div class="card">
                        <div class="card-header">
                            <h4>Update Announcement</h4>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('doctor.announcements.update',$announcement->id) }}"
                                  enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Level</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('level') is-invalid @enderror"
                                                name="level">
                                            @foreach($levels as $level)
                                                <option {{$announcement->level==$level->label?' selected':''}}>{{$level->label}}</option>
                                            @endforeach
                                        </select>
                                        @error('level')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('level') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                                           for="text">Announcement</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('text') is-invalid @enderror"
                                                  id="text" name="text"
                                                  rows="4">{{old('text',$announcement->text)}}</textarea>
                                        @error('text')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('text') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
