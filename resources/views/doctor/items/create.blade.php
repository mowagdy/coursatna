@extends('layouts.doctor-master')

@section('title','Create Item')

@section('content')
    <section class="section">
        <div class="section-header"><h1>Add Item</h1></div>
        <div class="section-body">

            <div class="row">
                <div class="col-12">
                    @include('doctor.common._alert_message')
                    @include('doctor.common._alert_validation_errors')
                    <div class="card">
                        <div class="card-header">
                            <h4>Add a New Item</h4>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('doctor.items.store') }}"
                                  enctype="multipart/form-data">
                                @csrf

                                {{-- Name --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                                    >Item Name</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="name" value="{{old('name')}}"
                                               class="form-control @error('name') is-invalid @enderror">
                                        @error('name')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('name') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Type --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Type</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('type') is-invalid @enderror"
                                                name="type">
                                            <option>BOOK</option>
                                            <option>SHEET</option>
                                            <option>SUMMARY</option>
                                            <option>OTHER</option>
                                        </select>
                                        @error('type')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('type') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Level --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Level</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('level') is-invalid @enderror"
                                                name="level" id="levels_list">
                                            @foreach($levels as $level)
                                                <option>{{$level->label}}</option>
                                            @endforeach
                                        </select>
                                        @error('level')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('level') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Univerity Subjects --}}
                                <div class="form-group row mb-4 item_university">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                                    >University Subject</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control @error('university_subject_id') is-invalid @enderror"
                                                name="university_subject_id" id="university_subjects_list">
                                            @foreach($universitySubjects as $universitySubject)
                                                <option value="{{$universitySubject->id}}">{{$universitySubject->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('university_subject_id')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('university_subject_id') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- File --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">File</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="file" value="{{old('file')}}"
                                               class="form-control @error('file') is-invalid @enderror">
                                        @error('file')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('file') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Image --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Image</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="file" name="image"
                                               class="form-control @error('image') is-invalid @enderror">
                                        @error('image')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('image') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Price --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Price</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="number" step="0.01" name="price" value="{{old('price')}}"
                                               class="form-control @error('price') is-invalid @enderror">
                                        @error('price')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('price') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                {{-- Description --}}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"
                                           for="description">Description</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="form-control @error('description') is-invalid @enderror"
                                                  id="description" name="description"
                                                  rows="4">{{old('description')}}</textarea>
                                        @error('description')
                                        <div class="invalid-feedback">
                                            <p>{{ $errors->first('description') }}</p>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                    <div class="col-sm-12 col-md-7">
                                        <button type="submit" class="btn btn-primary">Save New Item</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
