@extends('layouts.doctor-master')

@section('title','View Paid Transactions')

@section('content')
    <section class="section">
        <div class="section-header"><h1>View Paid Transactions</h1></div>
        <div class="section-body">

            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Paid Transactions <span>({{ $total }})</span></h4>
                            <div class="card-header-action">
                                <a href="{{route('doctor.items.show',$item->id)}}"
                                   class="btn btn-primary">Back to: {{$item->name}}</a>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive table-invoice">
                                @if(count($transactions)>0)
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <th>National ID</th>
                                            <th>Amount</th>
                                            <th>Paid At</th>
                                        </tr>
                                        @foreach($transactions as $transaction)
                                            <tr>
                                                <td>{{ $transaction->user->name }}</td>
                                                <td>{{ $transaction->user->national_id }}</td>
                                                <td>{{ $transaction->amount }}</td>
                                                <td>{{ $transaction->paid_at }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <div class="text-center p-3 text-muted">
                                        <h5>No Results</h5>
                                        <p>Looks like you don't have any transactions yet!</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    @if(count($transactions)>0)
                        <div class="text-center">
                            {{$transactions->links()}}
                        </div>
                    @endif
                </div>
            </div>

        </div>
    </section>
@endsection
