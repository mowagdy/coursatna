<?php

namespace App\Http\Requests\Api\Settings;

use App\Helpers\MoPhone;
use App\Http\Requests\Api\ApiMasterRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfileUpdateRequest extends ApiMasterRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if ($this->has('phone')) {

            $phone = new MoPhone($this->phone);
            if (!$phone->isValid()) {
                throw new HttpResponseException(response()->json([
                    'field' => 'phone',
                    'message' => $phone->errorMsg()
                ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY)); // 422
            }

            $this->merge(['phone' => $phone->getNormalized()]);
        }
    }

    public function rules(Request $request)
    {
        $rules = [
            'name' => 'nullable|string|max:100',
            'email' => 'nullable|email|max:100|unique:users,email,' . $request->user()->id,
            'phone' => 'nullable|string|max:50|unique:users,phone,' . $request->user()->id,
            'national_id' => 'nullable|numeric',
            'national_name' => 'nullable|string|min:10|max:190',
            'avatar' => 'nullable|mimes:png,jpg,jpeg',
            'cover_photo' => 'nullable|mimes:png,jpg,jpeg',
            'gender' => 'nullable|string|in:MALE,FEMALE',
            'role' => 'required|in:UNIVERSITY_STUDENT,SCHOOL_STUDENT',
            'bio' => 'nullable|string|max:500',
            'is_school_student' => 'nullable|boolean',
            'level' => 'nullable|string|max:100',
            'governorate_id' => 'nullable|numeric|exists:governorates,id',
            'city_id' => 'nullable|numeric|exists:cities,id',
            'locale' => 'nullable|string|max:3',
            'fcm_token' => 'required|string',
            'notification_toggle' => 'nullable|boolean',
        ];

        if ($request->has('role') && $request['role'] == 'UNIVERSITY_STUDENT') {
            $rules['field_id'] = 'nullable|numeric|exists:fields,id';
            $rules['faculty_id'] = 'nullable|numeric|exists:faculties,id';
            $rules['major_id'] = 'nullable|numeric|exists:majors,id';
        }

        if ($request->has('role') && $request['role'] == 'SCHOOL_STUDENT') {
            $rules['school_name'] = 'nullable|string|max:100';
        }

        return $rules;
    }
}
