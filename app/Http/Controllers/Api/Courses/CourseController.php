<?php

namespace App\Http\Controllers\Api\Courses;

use App\Http\Resources\Course\CourseDTO;
use App\Http\Resources\Course\CourseResource;
use App\Models\Course;
use App\Http\Controllers\Controller;

class CourseController extends Controller
{
    public function index()
    {
        return CourseDTO::collection(Course::paginate());
    }

    public function show(Course $course)
    {
        return CourseResource::make($course);
    }
}
