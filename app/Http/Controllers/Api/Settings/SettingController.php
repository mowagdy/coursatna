<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Settings\LevelUpdateRequest;
use App\Http\Requests\Api\Settings\ProfileUpdateRequest;
use App\Http\Requests\Api\Settings\UploadAvatarRequest;
use App\Http\Resources\AuthUsers\StudentUserDTO;

class SettingController extends Controller
{
    public function uploadAvatar(UploadAvatarRequest $request)
    {
        return response()->json([
            "avatar" => FileService::upload($request->file('avatar'), $request->user(), "avatars", true)
        ], 200);
    }

    public function updateProfile(ProfileUpdateRequest $request)
    {
        $user = $request->user();
        $data = $request->validated();
        $data['bio'] = $request['overview'];
        $user->update($data);

        return response()->json(new AuthedEmployerDTO($user), 200);
    }

    public function updateLevel(LevelUpdateRequest $request)
    {
        $user = $request->user();
        $user->update($request->validated());

        return response()->json(new StudentUserDTO($user), 200);
    }

}
