<?php

namespace App\Http\Controllers\Api\General;

use App\Http\Resources\General\LevelDTO;
use App\Models\Level;
use App\Http\Controllers\Controller;

class LevelController extends Controller
{
    public function index()
    {
        return response()->json(
            LevelDTO::collection(Level::all()),
            200
        );
    }
}
