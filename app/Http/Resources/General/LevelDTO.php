<?php

namespace App\Http\Resources\General;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LevelDTO extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'educational_level' => [
                'en' => $this->educational_level_en,
                'ar' => $this->educational_level_ar,
            ],
            'grade' => [
                'en' => $this->grade_en,
                'ar' => $this->grade_ar,
            ],
        ];
    }
}
