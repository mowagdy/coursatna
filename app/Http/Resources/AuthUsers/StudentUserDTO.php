<?php

namespace App\Http\Resources\AuthUsers;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentUserDTO extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email ?? "",
            'phone' => $this->phone ?? "",
            'birthday' => $this->birthday ?? "",
            'avatar_link' => $this->avatarLink,
            'cover_photo_link' => $this->cover_photo_link,
            'gender' => $this->gender ?? "",
            'bio' => $this->bio ?? "",
            'level' => [
                'id' => $this->level ? (int)$this->level->id : 0,
                'educational_level' => [
                    'en' => $this->level ? $this->level->educational_level_en : "",
                    'ar' => $this->level ? $this->level->educational_level_ar : "",
                ],
                'grade' => [
                    'en' => $this->level ? $this->level->grade_en : "",
                    'ar' => $this->level ? $this->level->grade_ar : "",
                ],
            ],
            'governorate' => [
                'id' => $this->governorate ? (int)$this->governorate->id : 0,
                'name' => [
                    'en' => $this->governorate ? $this->governorate->name_en : "",
                    'ar' => $this->governorate ? $this->governorate->name_ar : "",
                ],
            ],
            'city' => [
                'id' => $this->city ? (int)$this->city->id : 0,
                'name' => [
                    'en' => $this->city ? $this->city->name_en : "",
                    'ar' => $this->city ? $this->city->name_ar : "",
                ],
            ],
        ];
    }
}
