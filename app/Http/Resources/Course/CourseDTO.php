<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\AuthUsers\StudentUserDTO;
use App\Http\Resources\General\LevelDTO;
use App\Http\Resources\General\SubjectDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseDTO extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
//        'name',
//        'teacher_id',
//        'subject_id',
//        'level_id',
//        'image',
//        'price',
        return [
            'id' => (int)$this->id,
            'name' => $this->name,
            'teacher' => $this->teacher->name,
            'subject' => SubjectDTO::make($this->subject),
            'level' => LevelDTO::make($this->level),
            'image' =>$this->image? asset('/media/images/').$this->image : asset('/media/images/default.png'),
            'price' => (int)$this->price,
            'rate'=>0
        ];
    }
}
