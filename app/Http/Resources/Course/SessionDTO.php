<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\AuthUsers\StudentUserDTO;
use App\Http\Resources\General\LevelDTO;
use App\Http\Resources\General\SubjectDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class SessionDTO extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
//        'topic',
//        'uploader_id',
//        'course_id',
//        'open',
//        'file_type',
//        'file',
//        'external_url',
//        'status',
        return [
            'id' => (int)$this->id,
            'topic' => $this->topic,
            'open' => (int)$this->open,
            'file_type' => $this->file_type,
            'file' =>$this->file? asset('/media/files/').$this->file : asset('/media/images/file.pdf'),
        ];
    }
}
