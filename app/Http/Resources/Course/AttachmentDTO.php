<?php

namespace App\Http\Resources\Course;

use App\Http\Resources\AuthUsers\StudentUserDTO;
use App\Http\Resources\General\LevelDTO;
use App\Http\Resources\General\SubjectDTO;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AttachmentDTO extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
//        'course_id',
//        'uploader_id',
//        'name',
//        'file_type',
//        'external_url',
//        'file',
//        'image',
//        'price',
//        'status',
        return [
            'id' => (int)$this->id,
            'name' => $this->name,
            'file_type' => $this->file_type,
            'file' =>$this->file? asset('/media/files/').$this->file : asset('/media/images/file.pdf'),
            'image' =>$this->image? asset('/media/images/').$this->image : asset('/media/images/default.png'),
            'price' => (int)$this->price,
        ];
    }
}
