<?php

namespace App\Http\Enums;

final class UserRoles
{
    public const ROLE_SUPER_ADMIN = "SUPER_ADMIN";
    public const ROLE_ADMIN = "ADMIN";
    public const ROLE_USER = "TEACHER";
    public const ROLE_STUDENT = "STUDENT";
}
