<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $fillable = [
        'educational_level_en',
        'educational_level_ar',
        'grade_en',
        'grade_ar',
    ];
}
