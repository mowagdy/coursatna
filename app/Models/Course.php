<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name',
        'teacher_id',
        'subject_id',
        'level_id',
        'image',
        'price',
        'status',
    ];

    public function teacher()
    {
        return $this->belongsTo(User::class, "teacher_id");
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
    public function level()
    {
        return $this->belongsTo(Level::class);
    }
    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }
    public function sessions()
    {
        return $this->hasMany(CourseSession::class);
    }

    // rate
}
