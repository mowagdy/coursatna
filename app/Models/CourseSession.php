<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseSession extends Model
{
    protected $fillable = [
        'topic',
        'uploader_id',
        'course_id',
        'open',
        'file_type',
        'file',
        'external_url',
        'status',
    ];

    public function uploader()
    {
        return $this->belongsTo(User::class, "uploader_id");
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
