<?php

use Illuminate\Database\Seeder;

class LevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rows = [
            [
                'educational_level_en' => "Preparatory",
                'educational_level_ar' => "الإعدادي",
                'grade_en' => "First Grade",
                'grade_ar' => "الصف الأول",
            ],
            [
                'educational_level_en' => "Preparatory",
                'educational_level_ar' => "الإعدادي",
                'grade_en' => "Second Grade",
                'grade_ar' => "الصف الثاني",
            ],
            [
                'educational_level_en' => "Preparatory",
                'educational_level_ar' => "الإعدادي",
                'grade_en' => "Third Grade",
                'grade_ar' => "الصف الثالث",
            ],
            [
                'educational_level_en' => "High",
                'educational_level_ar' => "الثانوي",
                'grade_en' => "First Grade",
                'grade_ar' => "الصف الأول",
            ],
            [
                'educational_level_en' => "High",
                'educational_level_ar' => "الثانوي",
                'grade_en' => "Second Grade",
                'grade_ar' => "الصف الثاني",
            ],
            [
                'educational_level_en' => "High",
                'educational_level_ar' => "الثانوي",
                'grade_en' => "Third Grade",
                'grade_ar' => "الصف الثالث",
            ],
        ];

        $this->seedRows($rows);
    }

    private function seedRows($rows)
    {
        foreach ($rows as $row) {
            DB::table('levels')->updateOrInsert($row);
        }
    }
}
